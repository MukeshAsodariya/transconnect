import * as React from 'react';
import { Image, View, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';


const Splash = ({navigation}) => {
    React.useEffect(async () => {
        const appData = await AsyncStorage.getItem('isAppFirstLaunched');
        setTimeout(() => {0
          if (appData == null) {
            //   setIsAppFirstLaunched(true);
            AsyncStorage.setItem('isAppFirstLaunched', 'false');
            navigation.replace('OnboardingScreen')
            
            } 
            else {
            //   setIsAppFirstLaunched(false);
              navigation.replace('HomeScreen')
            }
        }, 3000);
    
        // AsyncStorage.removeItem('isAppFirstLaunched');
    }, []);
    return (
      <View style={{ width: '100%', height: '100%', alignItems:'center', justifyContent:'center' }}>
        <Image
               style={{width: 260, height:208}}
               source={require('../images/Splash.png')}/>
      </View>
    );
  };


  export default Splash;