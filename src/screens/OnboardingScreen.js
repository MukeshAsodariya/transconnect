import React from 'react';
import {
  SafeAreaView,
  Image,
  StyleSheet,
  FlatList,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';


const {width, height} = Dimensions.get('window');

const COLORS = {primary: '#fff', white: '#000', gray: '#808080', blue: '#734F96'};

const slides = [
  {
    id: '1',
    image: require('../images/image1.png'),
    title: 'Embrace your glorious authentic self',
    subtitle: '“All of us are put in boxes by our family, by our religion, by our society, our moment in history, even our own bodies. Some people have the courage to break free.” - Geena Rocero',
  },
  {
    id: '2',
    image: require('../images/image2.png'),
    title: 'We are born as who we are',
    subtitle: '"We are not what other people say we are. We are who we know ourselves to be, and we are what we love. That’s OK. You’re not alone in who you are." - Laverne Cox',
  },
  {
    id: '3',
    image: require('../images/image3.png'),
    title: 'Every. Single. Person. Deserves. To. Be. Treated. Equally.',
    subtitle: '',
  },
];

const Header = () => {
  return (
    <Image
     source={require('../images/TransConnect.png')}
     style={{height: '2.5%', width, top: 20, resizeMode: 'contain'}}
   />
  );
};

// const Slide = ({item}) => {
//   return (
//     <View style={{alignItems: 'center', marginTop: 50}}>
//       <Header />
//       <Image
//         source={item?.image}
//         style={{height: '75%', width, resizeMode: 'contain'}}
//       />
//       <View>
//         <Text style={styles.title}>{item?.title}</Text>
//         <Text style={styles.subtitle}>{item?.subtitle}</Text>
//       </View>
//       <Footer />
//     </View>
//   );
// };

const OnboardingScreen = ({navigation}) => {
  const [currentSlideIndex, setCurrentSlideIndex] = React.useState(0);
  
  const ref = React.useRef();
  const updateCurrentSlideIndex = e => {
    const contentOffsetX = e.nativeEvent.contentOffset.x;
    const currentIndex = Math.round(contentOffsetX / width);
    setCurrentSlideIndex(currentIndex);
  };

  const goToNextSlide = () => {
    // if (nextSlideIndex != slides.length) {
    //   const offset = nextSlideIndex * width;
    //   ref?.current.scrollToOffset({offset});
      setCurrentSlideIndex(currentSlideIndex + 1);
    // }
  };

  const goToPreviousSlide = () => {
    // if (nextSlideIndex != slides.length) {
    //   const offset = nextSlideIndex * width;
    //   ref?.current.scrollToOffset({offset});
      setCurrentSlideIndex(currentSlideIndex - 1);
    // }
  };

  const skip = () => {
    const lastSlideIndex = slides.length - 1;
    // const offset = lastSlideIndex * width;
    // ref?.current.scrollToOffset({offset});
    setCurrentSlideIndex(lastSlideIndex);
  };

  const Footer = () => {
    return (
      <View
        style={{
          height: 150,
          justifyContent: 'space-between',
          paddingHorizontal: 20,
        }}>
           
        {/* Indicator container */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 20,
          }}>
          {/* Render indicator */}
          {slides.map((_, index) => (
            <View
              key={index}
              style={[
                styles.indicator,
                currentSlideIndex == index && {
                  backgroundColor: COLORS.white,
                  width: 25,
                },
              ]}
            />
          ))}
        </View>

        {/* Render buttons */}
        <View style={{marginBottom: 20}}>
          {currentSlideIndex == slides.length - 1 ? (
            <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={[
                styles.btn,
                {
                  borderColor: COLORS.blue,
                  borderWidth: 1,
                  backgroundColor: 'transparent',
                },
              ]}
              onPress={goToPreviousSlide}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 15,
                  color: COLORS.blue,
                }}>
                PREVIOUS
              </Text>
            </TouchableOpacity>
            <View style={{width: 15}} />
            <TouchableOpacity
                style={styles.btn}
                onPress={() => navigation.replace('HomeScreen')}>
                <Text style={{fontWeight: 'bold', color: 'white', fontSize: 15}}>
                  GET STARTED
                </Text>
              </TouchableOpacity>
          </View>
          ) : (
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                activeOpacity={0.8}
                disabled={currentSlideIndex == 0}
                style={[
                  styles.btn,
                  {
                    borderColor: currentSlideIndex == 0 ? COLORS.gray : COLORS.blue,
                    borderWidth: 1,
                    backgroundColor: 'transparent',
                  },
                ]}
                onPress={goToPreviousSlide}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 15,
                    color: currentSlideIndex == 0 ? COLORS.gray : COLORS.blue,
                  }}>
                  PREVIOUS
                </Text>
              </TouchableOpacity>
              <View style={{width: 15}} />
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={goToNextSlide}
                style={styles.btn}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: 'white',
                    fontSize: 15,
                  }}>
                  NEXT
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  };

  const Slide = ({item}) => {
    return (
      <View style={{alignItems: 'center', marginTop: 50}}>
        <Header />
        <Image
          source={item?.image}
          style={{height: '75%', width, resizeMode: 'contain'}}
        />
        <View>
          <Text style={styles.title}>{item?.title}</Text>
          {/* <Text style={styles.subtitle}>{item?.subtitle}</Text> */}
        </View>
        <Footer />
      </View>
    );
  };

  return (
    <ScrollView style={{flex: 1, backgroundColor: COLORS.primary}}>
      <StatusBar backgroundColor={COLORS.primary} />
      <Image
        source={require('../images/TransConnect.png')}
        style={{height: 100, marginLeft:'20%' ,width: '60%', marginTop:40, resizeMode: 'contain'}}
      />
      <Image
          source={slides[currentSlideIndex]?.image}
          style={{height: '60%',width: '100%', resizeMode: 'contain'}}
        />
        <View>
          <Text style={styles.title}>{slides[currentSlideIndex]?.title}</Text>
          <Text style={styles.subtitle}>{slides[currentSlideIndex]?.subtitle}</Text>
        </View>
      <Footer />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  subtitle: {
    color: COLORS.white,
    fontSize: 13,
    marginTop: 10,
    maxWidth: '70%',
    // textAlign: 'center',
    marginLeft: '5%',
    lineHeight: 23,
  },
  title: {
    color: COLORS.white,
    fontSize: 22,
    fontWeight: 'bold',
    marginTop: 20,
    // textAlign: 'center',
    marginLeft: '5%',
  },
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  indicator: {
    height: 8,
    width: 10,
    backgroundColor: '#734F96',
    marginHorizontal: 3,
    borderRadius: 2,
  },
  btn: {
    flex: 1,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#734F96',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default OnboardingScreen;
