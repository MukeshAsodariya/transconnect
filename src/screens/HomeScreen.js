import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Dimensions, FlatList, TouchableOpacity, SafeAreaView, Linking} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { WebView } from 'react-native-webview';

import feed1Image from '../images/OB1.jpg'
import feed2Image from '../images/OB2.jpg'
import event1Image from '../images/Event1.png'
import event2Image from '../images/Event2.png'
import event3Image from '../images/Event3.png'
import event4Image from '../images/Event4.png'
import event5Image from '../images/Event5.png'

const feed1ImageUri = Image.resolveAssetSource(feed1Image).uri
const feed2ImageUri = Image.resolveAssetSource(feed2Image).uri
const event1ImageUri = Image.resolveAssetSource(event1Image).uri
const event2ImageUri = Image.resolveAssetSource(event2Image).uri
const event3ImageUri = Image.resolveAssetSource(event3Image).uri
const event4ImageUri = Image.resolveAssetSource(event4Image).uri
const event5ImageUri = Image.resolveAssetSource(event5Image).uri

const {width, height} = Dimensions.get('window');
const COLORS = {primary: '#fff', Black: '#000', gray: '#808080', blue: '#734F96'};

const DATA = [
  {
    id: 1,
    postTitle: 'Kerala initiates steps for free sex reassignment surgeries at medical college hospitals.',
    imageURI:
      feed1ImageUri,
    story:
      `The order by the Kerala State Human Rights Commission has lead to the State Government to initiate steps to provide adequate facilities and services of expert doctors for sex reassignment surgeries to transgender persons in hospitals. This includes the Government Medical College Hospital (MCH), Kottayam, An official statement on Read More...`
  },
  {  
    id: 2,
    postTitle: 'In A First, Karnataka Introduces Quota For ‘Male Third Gender’ in Police Constable Recruitment',
    imageURI:
      feed2ImageUri,
      story:
      `In a first for the state, the Karnataka government has announced reservation for ‘male third gender’ in recruitment to the state armed forces. State Home Minister Araga Jnanendra on Tuesday said the process to recruit constables to fill 3,484 posts in the Karnataka Armed Forces has started. “For the first time in the state 79 posts have been reserved for the ‘male third gender’,” he said Transgender activists hailed the move to provide quota. “I welcome the decision,” Read More...`
  }
]
const DATA2 = [
  {
    id: 1,
    imageURI:
      event1ImageUri,
    title:
      `Kashish Film Festival`,
    web: 'http://mumbaiqueerfest.com'
  },
  {  
    id: 2,
    imageURI:
      event2ImageUri,
    title:
      `Aravani Art Project`,
    web: 'https://aravaniartproject.com/' 
  },
  {  
    id: 3,
    imageURI:
      event3ImageUri,
    title:
      `QueerKala`,
    web: 'https://lgbtq.co.in/qb/queerkala/' 
  },
  {  
    id: 4,
    imageURI:
      event4ImageUri,
    title:
      `Sappho for Equality`,
    web: 'https://www.bqff.in/' 
  },
  {  
    id: 5,
    imageURI:
      event5ImageUri,
    title:
      `Association for the Transgender Health in India`,
    web: 'https://www.bqff.in/' 
  },
]

const renderItem = ({ item }) => (
  <View style={styles.card}>
     {/* <View style={styles.cardHeader}>
     <Image
          source={{ uri: item.imageURI }}
          style={styles.cardAvatar}
        />
      <Text category='s1' style={styles.cardTitle}>
        {item.postTitle}
      </Text>
         
    </View> */}
    {/* <Image
      source={{ uri: item.imageURI }}
      style={styles.cardImage}
    /> */}
    {/* <View style={styles.cardContent}> */}
    <Text category='p2' style={{fontWeight: 'bold', marginBottom: 10}}>{item.postTitle}</Text>
      <Text category='p2' style={{marginBottom: 10}}>{item.story}</Text>
      <Image
        source={{ uri: item.imageURI }}
        style={styles.cardImage}
      />
    {/* </View> */}
  </View>
)

const renderItem2 = ({ item }) => (
  <View style={styles.card2}>
    <Image
      source={{ uri: item.imageURI }}
      style={{ height: 82, width: 76 }}
    />
    
     {/* <View style={styles.cardHeader}>
     <Image
          source={{ uri: item.imageURI }}
          style={styles.cardAvatar}
        />
      <Text category='s1' style={styles.cardTitle}>
        {item.postTitle}
      </Text>
         
    </View> */}
    {/* <Image
      source={{ uri: item.imageURI }}
      style={styles.cardImage}
    /> */}
    <View style={{ marginLeft:20, flex:1 }}>
    <Text category='p2' style={{fontWeight: 'bold', marginBottom: 10}}>{item.title}</Text>
    <Text category='p2' style={{color: 'blue'}}
      onPress={() => Linking.openURL(item.web)}>
        {item.web}
    </Text>
    </View>
  </View>
)

function Feeds() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
       {/* return ( */}
          <FlatList
            style={{flex: 1}}
            data={DATA}
            renderItem={renderItem}
            keyExtractor={DATA.id}
          />
        {/* ) */}
    </View>
  );
}

function Events() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
     <FlatList
            style={{flex: 1}}
            data={DATA2}
            renderItem={renderItem2}
            keyExtractor={DATA2.id}
          />
    </View>
  );
}

function Employment() {
  // return (
  //   <WebView
  //     style={styles.WebViewStyle}
  //     //Loading html file from project folder
  //     source={require('../images/Transconnect_Healthcare.html')}
  //     //Enable Javascript support
  //     javaScriptEnabled={true}
  //         //For the Cache
  //     domStorageEnabled={true}
  //   />
  // );

  // IF THERE IS NO INFORMATION 
    return (
      <View>
          <Text style={styles.title}>{'The Job Employment Lists is Coming soon!'}</Text>
          <Image
              source={require('../images/Employment.png')}
              style={{height: '60%', width, top: 20, resizeMode: 'contain'}}
          />
      </View>
    );
  }

  function Health() {
    return (
      <WebView
        style={styles.WebViewStyle}
        //Loading html file from project folder
        source={require('../images/Transconnect_Healthcare.html')}
        //Enable Javascript support
        javaScriptEnabled={true}
            //For the Cache
        domStorageEnabled={true}
      />
    );

    //IF THERE IS NO INFORMATION 
    // return (
    //   <View>
    //     <Text style={styles.title}>{'The Health Lists is Coming soon!'}</Text>
    //     <Image
    //           source={require('../images/Health.png')}
    //           style={{height: '60%', width, top: 20, resizeMode: 'contain'}}
    //       />
    //   </View>
    // );
  }

  function Information() {
    return (
      <WebView
        style={styles.WebViewStyle}
        //Loading html file from project folder
        source={require('../images/Transconnect_News.html')}
        //Enable Javascript support
        javaScriptEnabled={true}
            //For the Cache
        domStorageEnabled={true}
      />
    );
      
    // IF THERE IS NO INFORMATION 
    //   <View>
    //     <Text style={styles.title}>{'There is a lot of Information Coming soon!'}</Text>
    //     <Image
    //           source={require('../images/Information.png')}
    //           style={{height: '60%', width, top: 20, resizeMode: 'contain'}}
    //       />
    //   </View>
    // );
  }

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer  independent={true}>
      <Tab.Navigator>
        <Tab.Screen name="Feeds"
                    component={Feeds}
                    options={{             
                      // tabBarBackground : "#000",
                      tabBarIcon: ({size,focused,color}) => {
                        return (
                          <Image
                            style={{ width: size, height: size }}
                            source={ focused ? require('../images/ant-design_home-filled-Focussed.png') : require('../images/ant-design_home-filled.png')}
                          />
                        );
                      },
                    }} 
          />
        <Tab.Screen name="Events"
                    component={Events}
                    options={{
                      tabBarIcon: ({size,focused,color}) => {
                        return (
                          <Image
                            style={{ width: size, height: size }}
                            source={ focused ? require('../images/bi_calendar-event-fill-Focussed.png') : require('../images/bi_calendar-event-fill.png')}
                          />
                        );
                      },
                    }}
         />
        <Tab.Screen name="Employment"
                    component={Employment}
                    options={{
                      tabBarIcon: ({size,focused,color}) => {
                        return (
                          <Image
                            style={{ width: size, height: size }}
                            source={focused ? require('../images/ic_outline-work-Focussed.png') : require('../images/ic_outline-work.png')}
                          />
                        );
                      },
                    }}
         />
        <Tab.Screen name="Health"
                    component={Health}
                    options={{
                      tabBarIcon: ({size,focused,color}) => {
                        return (
                          <Image
                            style={{ width: size, height: size }}
                            source={focused ? require('../images/game-icons_health-normal-Focussed.png') : require('../images/game-icons_health-normal.png')}
                          />
                        );
                      },
                    }}
         />
        <Tab.Screen name="Information"
                    component={Information}
                    options={{
                      tabBarIcon: ({size,focused,color}) => {
                        return (
                          <Image
                            style={{ width: size, height: size }}
                            source={focused ? require('../images/mingcute_information-fill-Focussed.png') :require('../images/mingcute_information-fill.png')}
                          />  
                        );
                      },
                    }}
       />
      </Tab.Navigator>
    </NavigationContainer>
  );
};


const styles = StyleSheet.create({
    title: {
      color: COLORS.Black,
      fontSize: 22,
      fontWeight: 'bold',
      marginTop: 100,
      textAlign: 'center',
    },
    image: {
      height: '100%',
      width: '100%',
      resizeMode: 'contain',
    },
    container: {
      flex: 1
    },
    card: {
      backgroundColor: COLORS.primary,
      marginTop: 10,
      padding: 10,
      borderWidth: 0.25,
      marginHorizontal: 10,
      borderColor: COLORS.gray
    },
    card2: {
      flexDirection: 'row',
      backgroundColor: COLORS.primary,
      marginTop: 10,
      padding: 10,
      width: width - 20,
      borderWidth: 0.25,
      marginHorizontal: 10,
      borderColor: COLORS.gray
    },
    cardImage: {
      width: '100%',
      height: 300
    },
    cardHeader: {
      padding: 10,
      flexDirection: 'row',
      alignItems: 'center',
      // justifyContent: 'space-between'
    },
    cardTitle: {
      color: COLORS.Black
    },
    cardAvatar: {
      marginRight: 16,
      height: 30,
      width: 30, 
      borderRadius: 100
    },
    cardContent: {
      padding: 10,
      borderWidth: 1,
      borderColor: COLORS.gray
    },
    // WebViewStyle: {
    //   justifyContent: 'center',
    //   alignItems: 'center',
    //   flex: 0.5,
    //   marginTop: 30,
    // },
  });
